<?php namespace FBIGuild\Warcraft\Response;

use \FBIGuild\Warcraft\Response\MappableResponseInterface;

/**
 * Parent class for the Response classes to make supporting the ResponseInterface and
 * MappableResponseInterface easier.
 * 
 * @author Leon Rowland <leon@rowland.nl>
 * @package Response
 */
abstract class AbstractResponse implements ResponseInterface, MappableResponseInterface {
    
    /**
     * Holds all of the MappableResponseInterfaces with handling the extra
     * optional fields from the request.
     * @var array<MappableResponseInterface>
     */
    private $optionalClasses = array();
    
    /**
     * Specifies the field to assign the MappableResponseInterface to.
     * The $field must match the key name given from the additional response
     * field.
     * 
     * @access public
     * @param string $field
     * @param \FBIGuild\Warcraft\Response\MappableResponseInterface $response
     */
    public function with($field, MappableResponseInterface $response)
    {
        $this->optionalClasses[$field] = $response;
    }
    
    /**
     * Returns the assigned MappableResponseInterface for the passed
     * in optional $field
     * 
     * @access public
     * @param string $field
     * @return \FBIGuild\Warcraft\Response\MappableResponseInterface
     */
    public function getOptional($field)
    {
        return $this->optionalClasses[$field];
    }
    
    /**
     * Returns all the specified MappableResponseInterfaces and keys of
     * the fields they are attached to.
     * 
     * @access public
     * @return array<MappableResponseInterface>
     */
    public function allOptionals()
    {
        return $this->optionalClasses;
    }
}