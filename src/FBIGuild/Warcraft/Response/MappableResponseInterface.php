<?php namespace FBIGuild\Warcraft\Response;

/**
 * An interface to ensure that the response from the API will be correctly
 * mapped.
 * 
 * @author Leon Rowland <leon@rowland.nl>
 * @package Response
 */
interface MappableResponseInterface {
    
    /**
     * The passed in $responseObject can be a \stdClass or an array. This makes
     * it hard to assign type hinting.  This method should handle the response from 
     * the API and map to the class as needed.
     * 
     * @access public
     * @param \stdClass|array $responseObject
     * @return void
     */
    public function map($responseObject);
}
