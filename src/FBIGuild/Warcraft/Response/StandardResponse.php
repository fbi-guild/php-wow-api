<?php namespace FBIGuild\Warcraft\Response;

/**
 * A standard response. Can be used in situations where a Response class
 * doesnt exist and you just want the raw response from the request.
 * 
 * @author Leon Rowland <leon@rowland.nl>
 * @package Response
 */
class StandardResponse extends AbstractResponse {
    
    private $response;
    
    public function map($responseObject)
    {
        $this->response = $responseObject;
    }
    
    public function get()
    {
        return $this->response;
    }
}
