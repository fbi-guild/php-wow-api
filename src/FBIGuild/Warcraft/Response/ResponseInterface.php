<?php namespace FBIGuild\Warcraft\Response;

/**
 * Helper interface used by the current Response classes. Is not used by any other part
 * of the system currently.
 * 
 * @author Leon Rowland <leon@rowland>
 * @package Response
 */
interface ResponseInterface {
    
    /**
     * Specifies the field to assign the MappableResponseInterface to.
     * The $field must match the key name given from the additional response
     * field.
     * 
     * @access public
     * @param string $field
     * @param \FBIGuild\Warcraft\Response\MappableResponseInterface $response
     */
    public function with($field, MappableResponseInterface $response);
    
    /**
     * Returns the assigned MappableResponseInterface for the passed
     * in optional $field
     * 
     * @access public
     * @param string $field
     * @return \FBIGuild\Warcraft\Response\MappableResponseInterface
     */
    public function getOptional($field);
    
    /**
     * Returns all the specified MappableResponseInterfaces and keys of
     * the fields they are attached to.
     * 
     * @access public
     * @return array<MappableResponseInterface>
     */
    public function allOptionals();
}