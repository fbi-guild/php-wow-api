<?php namespace FBIGuild\Warcraft\Response\Character;

use \FBIGuild\Warcraft\Response\AbstractResponse;
use \FBIGuild\Warcraft\Response\MappableResponseInterface;

/**
 * Handles the character response from the API.
 * 
 * @author Leon Rowland <leon@rowland.nl>
 * @package Response
 * @subpackage Character
 */
class CharacterResponse extends AbstractResponse {
    
    /**
     * Total achievement points for the character
     * @var int
     */
    private $achievementPoints;
    
    /**
     * Current battlegroup for the character
     * @var string
     */
    private $battlegroup;
    
    /**
     * Letter identifier for the WoW site talent
     * calculator.
     * @var string 
     */
    private $calcClass;
    
    /**
     * Number identifier for the current class
     * of the character
     * @var int 
     */
    private $class;
    
    /**
     * Number identifier for the gender of
     * this character.
     * @var int 
     */
    private $gender;
    
    /**
     * Unix timestamp for the last modification
     * date of the character.
     * @var string
     */
    private $lastModified;
    
    /**
     * The level of the character
     * @var int
     */
    private $level;
    
    /**
     * The name of the character
     * @var string
     */
    private $name;
    
    /**
     * Number identifier for the race of the
     * character
     * @var int
     */
    private $race;
    
    /**
     * The realm the character is currently located
     * on.
     * @var string
     */
    private $realm;
    
    /**
     * The last URL component for the thumbnail
     * image of the character.
     * @var string
     */
    private $thumbnail;
    
    /**
     * The MappableResponseInterface that will handle the 
     * mapping of the data relating to the extra fields 
     * request of Guild.
     * @var \FBIGuild\Warcraft\Response\Character\Optional\Guild
     */
    private $guild;
    
    /**
     * The MappableResponseInterface that will handle the
     * mapping of the data relating to the extra fields
     * request of Titles.
     * @var \FBIGuild\Warcraft\Response\Character\Optional\Titles
     */
    private $titles;
    
    /**
     * Maps the response from the API to the properties of this class. Will
     * also get all requested options of the response and map those to their
     * respective classes.
     * 
     * @access public
     * @param stdClass $characterObject
     */
    public function map($characterObject) 
    {
        $this->achievementPoints = $characterObject->achievementPoints;
        $this->battleGroup       = $characterObject->battlegroup;
        $this->calcClass         = $characterObject->calcClass;
        $this->class             = $characterObject->class;
        $this->gender            = $characterObject->gender;
        $this->lastModified      = $characterObject->lastModified;
        $this->level             = $characterObject->level;
        $this->name              = $characterObject->name;
        $this->race              = $characterObject->race;
        $this->realm             = $characterObject->realm;
        $this->thumbnail         = $characterObject->thumbnail;
        
        // Assign all the specified optionals for this CharacterResponse
        foreach($this->allOptionals() as $optional => $optionalClass)
        {
            if(property_exists($characterObject, $optional))
            {
                $optionalClass->map($characterObject->$optional);
                $this->$optional = $optionalClass;
            }
        }
    }
    
    /**
     * Return the total achievement points for this character
     * 
     * @access public
     * @return int
     */
    public function getAchievementPoints() 
    {
        return $this->achievementPoints;
    }

    /**
     * Return the current battlegroup that the requested character
     * belongs to.
     * 
     * @access public
     * @return string
     */
    public function getBattlegroup() 
    {
        return $this->battlegroup;
    }

    /**
     * Return the talent calculator string identifier for the
     * class of this character.
     * 
     * @access public
     * @return string
     */
    public function getCalcClass() 
    {
        return $this->calcClass;
    }

    /**
     * Return the integer identifier for this characters class.
     * 
     * @access public
     * @return int
     */
    public function getClass() 
    {
        return $this->class;
    }

    /**
     * Returns the integer indentifier for this characters gender.
     * 
     * @access public
     * @return int
     */
    public function getGender() 
    {
        return $this->gender;
    }

    /**
     * Returns the unix timestamp for the last modification.
     * 
     * @access public
     * @return int
     */
    public function getLastModified() 
    {
        return $this->lastModified;
    }

    /**
     * Returns the level of the character.
     * 
     * @access public
     * @return int
     */
    public function getLevel() 
    {
        return $this->level;
    }

    /**
     * Returns the characters name.
     * 
     * @access public
     * @return string
     */
    public function getName() 
    {
        return $this->name;
    }

    /**
     * Returns the integer indentifier for this characters race.
     * 
     * @access public
     * @return int
     */
    public function getRace() 
    {
        return $this->race;
    }

    /**
     * Returns the realm of this character.
     * 
     * @access public
     * @return string
     */
    public function getRealm() 
    {
        return $this->realm;
    }

    /**
     * Returns the last URL part for the thumbnail of this 
     * character.
     * 
     * @access public
     * @return string
     */
    public function getThumbnail() 
    {
        return $this->thumbnail;
    }
    
    /**
     * Specifys the Guild class MappableResponseInterface for handling the extra
     * data from the optional guild request.
     * 
     * Default usage is with \FBIGuild\Warcraft\Response\Character\Optional\Guild
     * 
     * @access public
     * @param \FBIGuild\Warcraft\Response\MappableResponseInterface $guildResponse
     * @return \FBIGuild\Warcraft\Response\Character\CharacterResponse
     */
    public function withGuild(MappableResponseInterface $guildResponse)
    {
        $this->with('guild', $guildResponse);
        return $this;
    }
    
    /**
     * Returns the specified MappableResponseInterface from the withGuild method
     * that will have been populated by the request.
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Response\Character\Optional\Guild
     */
    public function getGuild()
    {
        return $this->guild;
    }
    
    /**
     * Specifies the Titles class MappableResponseInterface for handling the extra
     * data from the optional titles request.
     * 
     * Default usage is with \FBIGuild\Warcraft\Response\Character\Optional\Titles
     * 
     * @access public
     * @param \FBIGuild\Warcraft\Response\MappableResponseInterface $titleResponse
     * @return \FBIGuild\Warcraft\Response\Character\CharacterResponse
     */
    public function withTitles(MappableResponseInterface $titleResponse)
    {
        $this->with('titles', $titleResponse);
        return $this;
    }
    
    /**
     * Returns the specified MappableResponseInterface from the withTitles method
     * that will have been populated by the request.
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Response\Character\Optional\Titles
     */
    public function getTitles()
    {
        return $this->titles;
    }
}