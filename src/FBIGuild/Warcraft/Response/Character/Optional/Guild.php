<?php namespace FBIGuild\Warcraft\Response\Character\Optional;

use \FBIGuild\Warcraft\Response\MappableResponseInterface;

class Guild implements MappableResponseInterface {
    
    private $achievementPoints;
    private $battlegroup;
    private $emblem;
    private $level;
    private $members;
    private $name;
    private $realm;
    
    public function map($responseObject)
    {
        $this->achievementPoints = $responseObject->achievementPoints;
        $this->battlegroup       = $responseObject->battlegroup;
        $this->emblem            = $responseObject->emblem;
        $this->level             = $responseObject->level;
        $this->members           = $responseObject->members;
        $this->name              = $responseObject->name;
        $this->realm             = $responseObject->realm;
    }
    
    public function getAchievementPoints() 
    {
        return $this->achievementPoints;
    }

    public function getBattlegroup() 
    {
        return $this->battlegroup;
    }

    public function getEmblem() 
    {
        return $this->emblem;
    }

    public function getLevel() 
    {
        return $this->level;
    }

    public function getMembers() 
    {
        return $this->members;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRealm() 
    {
        return $this->realm;
    }


}