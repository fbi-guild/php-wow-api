<?php namespace FBIGuild\Warcraft\Response\Character\Optional;

use \FBIGuild\Warcraft\Response\MappableResponseInterface;

class Titles implements MappableResponseInterface, \Iterator {
    
    private $titles;
    
    public function map($responseObject)
    {
        $this->titles = $responseObject;
    }
    
    public function get()
    {
        return $this->titles;
    }

    public function current() 
    {
        return current($this->titles);
    }

    public function key() 
    {
        return key($this->titles);
    }

    public function next()
    {
        return next($this->titles);
    }

    public function rewind()
    {
        reset($this->titles);
    }

    public function valid()
    {
        return key($this->titles) !== null;
    }

}