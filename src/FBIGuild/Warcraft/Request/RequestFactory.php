<?php namespace FBIGuild\Warcraft\Request;

use \FBIGuild\Warcraft\Client\ClientInterface;
use \FBIGuild\Warcraft\Request\RequestInterface;
use \FBIGuild\Warcraft\Response\MappableResponseInterface;

/**
 * Handles making the request with the client.
 * 
 * @author Leon Rowland <leon@rowland.nl>
 * @package Request
 */
class RequestFactory {
    
    /**
     * Holds the prefered client class
     * 
     * @var \FBIGuild\Warcraft\Client\ClientInterface
     */
    private $client;
    
    /**
     * Requires a usage of the ClientInterface so we can make the requests
     * 
     * @access public
     * @param \FBIGuild\Warcraft\Client\ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
    
    /**
     * Makes a request to the API and return the defined ResponseInterface
     * 
     * @access public
     * @param \FBIGuild\Warcraft\Request\RequestInterface $request
     * @param \FBIGuild\Warcraft\Response\MappableResponseInterface $response
     * @return \FBIGuild\Warcraft\Response\ResponseInterface
     */
    public function send(RequestInterface $request, MappableResponseInterface $response)
    {
        // Make the request with the client interface.
        $apiResponse = $this->client->makeRequest($request);
        
        // Determine if the request was valid
        if($this->client->wasValid()) {
            $response->map($apiResponse);
            return $response;
        }
        
        return false;
    }
}