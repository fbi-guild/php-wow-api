<?php namespace FBIGuild\Warcraft\Request;

/**
 * Interface for using the requests inside the Client.
 * 
 * @author Leon FBIGuild <leon@FBIGuild.nl>
 * @package Request
 */
interface RequestInterface {
    
    /**
     * Returns the full request URL to send to the
     * wow api.
     */
    public function generateRequestUrl();
    
    /**
     * Returns a boolean depending on wether the
     * request has additional fields
     */
    public function hasFields();
}