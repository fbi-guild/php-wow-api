<?php namespace FBIGuild\Warcraft\Request;

/**
 * Make a request with the character class.  Has many methods available for
 * requesting more information of the character.
 * 
 * Requires the realm and character name passed into the constructor.
 * 
 * Look for the methods with*.  All with* methods are fluent, so you can
 * chain them if you want.
 * 
 * @author Leon FBIGuild <leon@FBIGuild.nl>
 * @package Request
 * @subpackage Character
 */
class CharacterRequest extends AbstractRequest {
    
    /**
     * Holds the realm of the character
     * request
     * 
     * @access private
     * @var string
     */
    private $realm;
    
    /**
     * Holds the character name for
     * the request
     * 
     * @access private
     * @var string
     */
    private $characterName;
    
    /**
     * Pass in the realm and character name for this
     * character request.
     * 
     * Then look to any additional data you require from the
     * public with* methods.
     * 
     * If no extra with methods are called, the response is
     * a base response.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api
     * 
     * @param string $realm
     * @param string $characterName
     */
    public function __construct($realm, $characterName) 
    {
        // Set the requestUrl for the Character Request
        $this->setRequestUrl("api/wow/character/$realm/$characterName");
        
        // Set the Character properties
        $this->realm         = $realm;
        $this->characterName = $characterName;
    }
    
    /**
     * Adds a map of achievement data including completion timestamps
     * and criteria information.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/achievements
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withAchievements()
    {
        $this->with('achievements');
        return $this;
    }
    
    /**
     * Adds a map of values that describes the face, features and
     * helm/cloak display preferences and attributes.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/appearance
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withAppearance()
    {
        $this->with('appearance');
        return $this;
    }
    
    /**
     * Adds the activity feed of the character.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/feed
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withFeed()
    {
        $this->with('feed');
        return $this;
    }
    
    /**
     * Adds a summary of the guild that the character belongs to. If the character
     * does not belong to a guild and this field is requests, this field will
     * not be exposed.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/guild
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withGuild()
    {
        $this->with('guild');
        return $this;
    }
    
    /**
     * Adds a list of all the combat pets obtained by the character.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/hunterPets
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withHunterPets()
    {
        $this->with('hunterPets');
        return $this;
    }
    
    /**
     * Adds a list of items equiped by the character. Use of this field will also
     * include the average item level and average item level equipped for the
     * character.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/items
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withItems()
    {
        $this->with('items');
        return $this;
    }
    
    /**
     * Adds a list of all of the mounts obtained by the character.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/mounts
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withMounts()
    {
        $this->with('mounts');
        return $this;
    }
    
    /**
     * Adds a list of all the battle pets obtained by the character.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/pets
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withPets()
    {
        $this->with('pets');
        return $this;
    }
    
    /**
     * Adds data about the current battle pet slots on this characters
     * account.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/petSlots
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withPetSlots()
    {
        $this->with('petSlots');
        return $this;
    }
    
    /**
     * Adds a list of all the characters professions. It is important to that that
     * when this information is retrieved, it will also include the known receipes
     * of each of the listed professions.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/professions
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withProfessions()
    {
        $this->with('professions');
        return $this;
    }
    
    /**
     * Adds a list of all raids and bosses indicating raid progression and completedness.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/progression
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withProgression()
    {
        $this->with('progression');
        return $this;
    }
    
    /**
     * Adds a map of pvp information including arena team membership and rated 
     * battlegrounds information.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/pvp
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withPvp()
    {
        $this->with('pvp');
        return $this;
    }
    
    /**
     * Adds a list of quests completed by the character.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/quests
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withQuests()
    {
        $this->with('quests');
        return $this;
    }
    
    /**
     * Adds a list of the factions that the character has an associated 
     * reputation with.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/reputation
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withReputation()
    {
        $this->with('reputation');
        return $this;
    }
    
    /**
     * Adds a map of character attributes and stats.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/stats
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withStats()
    {
        $this->with('stats');
        return $this;
    }
    
    /**
     * Adds a list of talent structures.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/talents
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withTalents()
    {
        $this->with('talents');
        return $this;
    }
    
    /**
     * Adds a list of the titles obtained by the character including the currently
     * selected title.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/titles
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withTitles()
    {
        $this->with('titles');
        return $this;
    }
    
    /**
     * Adds raw character audit data that powers the character audit on the
     * game site.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#character-profile-api/audit
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Character\Character
     */
    public function withAudit()
    {
        $this->with('audit');
        return $this;
    }
}