<?php namespace FBIGuild\Warcraft\Request;

/**
 * Make a request with the guild class. Has many methods available for
 * requesting more information on the guild.
 * 
 * Requires the realm and guild name to be passed into the constructor.
 * 
 * Look for the methods with*. All with* methods are fluent, so you can
 * chain them if you want.
 * 
 * @author Leon FBIGuild <leon@FBIGuild.nl>
 * @package Request
 * @subpackage Guild
 */
class GuildRequest extends AbstractRequest {
    
    /**
     * Holds the realm of the guild
     * request.
     * 
     * @access private
     * @var string
     */
    private $realm;
    
    /**
     * Holds the guild name for
     * the request
     * 
     * @access private
     * @var string
     */
    private $guildName;
    
    /**
     * Pass in the realm and guild name for this
     * guild request.
     * 
     * Then look to any additional data you require from the
     * public with* methods.
     * 
     * If no extra with methods are call, the response is
     * a base response.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#guild-profile-api
     * 
     * @param string $realm
     * @param string $guildName
     */
    public function __construct($realm, $guildName)
    {
        $guildName = rawurlencode($guildName);
        $this->setRequestUrl("/api/wow/guild/$realm/$guildName");
        
        $this->realm = $realm;
        $this->guildName = $guildName;
    }
    
    /**
     * Adds a list of characters that are a member of this
     * guild.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#guild-profile-api/members
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Guild
     */
    public function withMembers()
    {
        $this->with('members');
        return $this;
    }
    
    /**
     * Adds a set of data structures that describe the achievements
     * earned by the guild.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#guild-profile-api/achievements
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Guild
     */
    public function withAchievements()
    {
        $this->with('achievements');
        return $this;
    }
    
    /**
     * Adds a set of data structures that describe the news feed 
     * of the guild.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#guild-profile-api/news
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Guild
     */
    public function withNews()
    {
        $this->with('news');
        return $this;
    }
    
    /**
     * Adds the top 3 challenge mode guild run times for each
     * challenge mode map.
     * 
     * @see http://blizzard.github.io/api-wow-docs/#guild-profile-api/challenge
     * 
     * @access public
     * @return \FBIGuild\Warcraft\Request\Guild
     */
    public function withChallenge()
    {
        $this->with('challenge');
        return $this;
    }
}