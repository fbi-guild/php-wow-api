<?php namespace FBIGuild\Warcraft\Request;

/**
 * Extend your child request classes with this abstract request
 * class to inherit some of the required methods to correctly 
 * send that child request to the API.
 * 
 * @author Leon FBIGuild <leon@FBIGuild.nl>
 * @package Request
 */
abstract class AbstractRequest implements RequestInterface {
    
    /**
     * Holds the full request url
     * that is part of the child
     * request.
     * 
     * @access private
     * @var string
     */
    private $requestUrl;
    
    /**
     * Holds all the additional fields
     * for this request.
     * 
     * @access private
     * @var array
     */
    private $fields = array();
    
    /**
     * Sets the additional request url part. You should
     * call this from inside one of the child Request classes
     * constructor.
     * 
     * @access public
     * @param string $requestUrl
     */
    protected function setRequestUrl($requestUrl) 
    {
        $this->requestUrl = $requestUrl;
    }
    
    /**
     * Adds an additional field to be added to the request. Your
     * child classes of AbstractRequest should use this method
     * inside their own wrapping methods to make it easier for 
     * the user.
     * 
     * @see \FBIGuild\Warcraft\Request\Character\Character.php
     * 
     * @access protected
     * @param string $field
     * @return void
     */
    protected function with($field)
    {
        $this->fields[] = $field;
    }
    
    /**
     * Generates the full requestUrl, including and the
     * specified extra fields for the request.
     * 
     * @access public
     * @return string
     */
    public function generateRequestUrl()
    {
        // Get the current request url
        $requestUrl = $this->requestUrl;
        
        // Get all fields
        if(!empty($this->fields))
        {
            $requestUrl .= '?fields=' . implode(',', $this->fields);
        }
        
        return $requestUrl;
    }
    
    /**
     * 
     * @access public
     * @return bool
     */
    public function hasFields()
    {
        return (!empty($this->fields));
    }
}