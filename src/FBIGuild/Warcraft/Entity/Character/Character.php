<?php namespace FBIGuild\Warcraft\Entity\Character;

use \FBIGuild\Warcraft\Response\CharacterResponse;

class Character {
    
    private $achievementPoints;
    private $battlegroup;
    private $calcClass;
    private $class;
    private $gender;
    private $lastModified;
    private $level;
    private $name;
    private $race;
    private $realm;
    private $thumbnail;
    
    /**
     * Maps the raw response from the API to this class. Is mainly used
     * in the Response class.
     * 
     * @access public
     * @param stdClass $characterResponse
     */
    public function map(CharacterResponse $characterResponse)
    {
        $this->achievementPoints = $characterResponse->getAchievements();
        $this->battleGroup       = $characterResponse->getBattlegroup();
        $this->calcClass         = $characterResponse->getCalcClass();
        $this->class             = $characterResponse->getClass();
        $this->gender            = $characterResponse->getGender();
        $this->lastModified      = $characterResponse->getLastModified();
        $this->level             = $characterResponse->getLevel();
        $this->name              = $characterResponse->getName();
        $this->race              = $characterResponse->getRace();
        $this->realm             = $characterResponse->getRealm();
        $this->thumbnail         = $characterResponse->getThumbnail();
        
    }
    
    /**
     * Returns all the achievement points for this character.
     * 
     * @access public
     * @return int
     */
    public function getAchievementPoints() 
    {
        return $this->achievementPoints;
    }

    /**
     * Returns the battlegroup that the realm of this character belongs
     * to.
     * 
     * @access public
     * @return string
     */
    public function getBattlegroup() 
    {
        return $this->battlegroup;
    }

    public function getCalcClass() 
    {
        return $this->calcClass;
    }

    /**
     * Returns the number identifier for the class of this
     * character.
     * 
     * @access public
     * @return int
     */
    public function getClass() 
    {
        return $this->class;
    }

    /**
     * Returns the number identifier for the gender of this
     * character.
     * 
     * @access public
     * @return int
     */
    public function getGender() 
    {
        return $this->gender;
    }

    /**
     * Returns a DateTime based on the last modified
     * timestamp.
     * 
     * @access public
     * @return DateTime
     */
    public function getLastModified() 
    {
        return DateTime::createFromFormat('U', $this->lastModified);
    }
    
    /**
     * Returns the raw response from the API. Is a 
     * timestamp.
     * 
     * @access public
     * @return string {timestamp}
     */
    public function getLastModifiedRaw()
    {
        return $this->lastModified;
    }

    /**
     * Returns the characters level
     * 
     * @access public
     * @return int
     */
    public function getLevel() 
    {
        return $this->level;
    }

    /**
     * Returns the characters name
     * 
     * @access public
     * @return string
     */
    public function getName() 
    {
        return $this->name;
    }

    /**
     * Returns the number identifier for the race of this character.
     * 
     * @access public
     * @return int
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Returns the full realm name for this character.
     * 
     * @access public
     * @return string
     */
    public function getRealm() 
    {
        return $this->realm;
    }

    /**
     * Returns the thumbnail link of the character. Requires a region to be passed in.
     * 
     * @access public
     * @param string $region [eu, us etc]
     * @return string
     */
    public function getThumbnail($region)
    {
        return sprintf('http://%s.battle.net/static-render/%s/%s', $region, $region, $this->thumbnail );
    }
}