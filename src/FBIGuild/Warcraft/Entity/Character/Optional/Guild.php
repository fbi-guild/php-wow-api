<?php namespace FBIGuild\Warcraft\Entity\Character;

class Guild {
    
    private $name;
    private $realm;
    private $battlegroup;
    private $level;
    private $members;
    private $achievementPoints;
    
    public function map($guildObject)
    {
        $this->name              = $guildObject->name;
        $this->realm             = $guildObject->realm;
        $this->battlegroup       = $guildObject->battlegroup;
        $this->level             = $guildObject->level;
        $this->members           = $guildObject->members;
        $this->achievementPoints = $guildObject->achievementPoints;
    }
    
    public function getName() {
        return $this->name;
    }

    public function getRealm() {
        return $this->realm;
    }

    public function getBattlegroup() {
        return $this->battlegroup;
    }

    public function getLevel() {
        return $this->level;
    }

    public function getMembers() {
        return $this->members;
    }

    public function getAchievementPoints() {
        return $this->achievementPoints;
    }
}