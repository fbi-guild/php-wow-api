<?php namespace FBIGuild\Warcraft\Client;

use \FBIGuild\Warcraft\Request\RequestInterface;

class Client implements ClientInterface {
    
    private $host;
    private $locale;
    
    private $handle;
    
    public function __construct($host = 'eu.battle.net', $locale = 'en_GB')
    {
        $this->host   = $host;
        $this->locale = $locale;
    }
    
    public function makeRequest(RequestInterface $request)
    {
        $this->handle = curl_init();
        
        if($request->hasFields())
        {
            $seperator = '&';
        } else {
            $seperator = '?';
        }
        
        // Create the full curl url
        $curlUrl = 'http://' . $this->host . '/' . $request->generateRequestUrl() . $seperator . 'locale=' . $this->locale;
        
        // Set the curl options
        curl_setopt_array($this->handle, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $curlUrl
        ));
        
        $this->response = curl_exec($this->handle);
        
        return json_decode($this->response);
    }
    
    public function getResponse()
    {
        return $this->response;
    }
    
    public function wasValid()
    {
        return (bool) (isset($this->response));
    }
}