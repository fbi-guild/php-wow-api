<?php namespace FBIGuild\Warcraft\Client;

use \FBIGuild\Warcraft\Request\RequestInterface;

/**
 * Interface example for the client that interacts with the
 * battle.net wow service.
 */
interface ClientInterface {
    
    /**
     * Make a request to the battlenet service.
     * 
     * @access public
     * @param RequestInterface The Request
     * @return string JSON
     */
    public function makeRequest(RequestInterface $interface);
    
    /**
     * Get the response from the last request.
     * 
     * @access public
     * @return string JSON string
     */
    public function getResponse();
}