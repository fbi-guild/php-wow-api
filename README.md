#PHP World of Warcraft (WoW) API
#### By Leon Rowland | http://leon.rowland.nl

### Description

Use the World of Warcraft Community API simply with this small library.  Not all features are finished yet. See the table below.

The purpose of this code is for a future guild website that will tie heavily into the World of Warcraft API
for aspects like Registration, News and useful member details.

### Usage

Lets say you wanted some character information. The general process is to make a request class
for what you want. Make a response class to handle the API response and then use the RequestFactory
to send the Request and map to the Response.

Get our Character request class.
```php
use \FBIGuild\Warcraft\Request\CharacterRequest;

$characterRequest = new CharacterRequest('Thunderhorn', 'Zogot');
```

This would respond with basic information about the class. See [here](http://blizzard.github.io/api-wow-docs/#character-profile-api).

If you wanted some additional fields, say the guild they are with and all their titles, you would do this

```php
$characterRequest
	->withGuild()
	->withTitles();
```

The Request is prepared, the next is to prepare the response!

```php
use \FBIGuild\Warcraft\Response\Character as Character;

$characterResponse = new Character\CharacterResponse();
```

This will handle the basic request only. If you have specified additional 'with*' methods then you also
need those on the $characterResponse

```php
$characterResponse
	->withGuild(new Character\Optional\Guild)
	->withTitles(new Character\Optional\Titles);
```

Now we are ready to make the request! First we need to make the client that will be used
to send the request.  The client requires the server you wish to query and the locale you
want it to pass.

```php
$client = new \FBIGuild\Warcraft\Client\Client('eu.battle.net', 'en_GB');
```

Now we make the RequestFactory and give it the new Client.

```php
use \FBIGuild\Warcraft\Request as Request;

$requestFactory = new Request\RequestFactory($client);
```

Now give it both the Request and Response classes we made earlier.

```php
$response = $requestFactory->send($characterRequest, $characterResponse);
```

The $response will now be an instance of what $characterResponse is, but with all the methods that
will give you basic values from the API.

I have started work on Entity usages that will take specific responses and give a more powerful class
to use. Example is the $characterResponse only gives the remaining string of the thumbnail, where as the
entities call to the thumbnail will provide a full working URL.

### State

#### Achievement [Read More](http://blizzard.github.io/api-wow-docs/#achievement-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Auction [Read More](http://blizzard.github.io/api-wow-docs/#auction-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### BattlePet [Read More](http://blizzard.github.io/api-wow-docs/#battlepet-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Challenge Mode [Read More](http://blizzard.github.io/api-wow-docs/#challenge-mode-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Character [Read More](http://blizzard.github.io/api-wow-docs/#character-profile-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
| :heavy_check_mark: | :heavy_check_mark: ||

##### Optional Fields

| | Request | Response | Entity |
| --- | :---: | :------: | :----: |
| Achievements ||||
| Appearance ||||
| Feed ||||
| Guild |:heavy_check_mark:|:heavy_check_mark:||
| Hunter Pets ||||
| Items ||||
| Mounts ||||
| Pets ||||
| Pet Slots ||||
| Professions ||||
| Progression ||||
| PVP ||||
| Quests ||||
| Reputation ||||
| Stats ||||
| Talents ||||
| Titles |:heavy_check_mark:|:heavy_check_mark:||
| Audit ||||

#### Item [Read More](http://blizzard.github.io/api-wow-docs/#item-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Guild [Read More](http://blizzard.github.io/api-wow-docs/#guild-profile-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
| :heavy_check_mark: |||

#### PVP [Read More](http://blizzard.github.io/api-wow-docs/#pvp-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Quest [Read More](http://blizzard.github.io/api-wow-docs/#quest-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Realm Status [Read More](http://blizzard.github.io/api-wow-docs/#realm-status-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Recipe [Read More](http://blizzard.github.io/api-wow-docs/#recipe-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Spell [Read More](http://blizzard.github.io/api-wow-docs/#spell-api)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

#### Data Resources [Read More](http://blizzard.github.io/api-wow-docs/#data-resources)

| Request | Response | Entity |
| :-----: | :------: | :----: |
||||

### Contributing

Feel free to contribute. If you look at the Character or Guild examples, you can see it is very simple.

